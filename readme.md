
libpeano
========

This is an implementation of a 3D peano curve that uses bitwise vectorization.
I had to finally write it so that I could stop thinking about it and get back
to work.
