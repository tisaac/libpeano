
#include "peano.h"
#include <stdio.h>

/* Take a number < 27**12, and separate it into base-27, grouped into five bit
 * (three packed trit) sections. */
peano_t
peano_from_digit_reference (uint64_t digit)
{
  peano_t peano = 0;
  for (int shift = 0; shift < 64; shift += 5) {
    uint64_t thismod = digit % 27;

    digit /= 27;
    peano |= (thismod << shift);
  }
  return peano;
}

/* Take a number < 27**12, and separate it into base-27, grouped into five bit
 * (three packed trit) sections. */
peano_t
peano_from_digit (uint64_t digit)
{
  uint64_t p27_6 = 387420489;
  uint64_t hi = digit / p27_6; /* hi has 18 trits */
  uint64_t lo = digit % p27_6; /* lo has 18 trits */
  uint32_t p27_3 = 19683;
  uint32_t hi_hi32 = (uint32_t) hi / p27_3; /* hi_hi has 9 trits */
  uint32_t hi_lo32 = (uint32_t) hi % p27_3; /* hi_lo has 9 trits */
  uint32_t lo_hi32 = (uint32_t) lo / p27_3; /* lo_hi has 9 trits */
  uint32_t lo_lo32 = (uint32_t) lo % p27_3; /* lo_lo has 9 trits */

  hi = (((uint64_t) hi_hi32) << 30) | (uint64_t) lo_hi32;
  lo = (((uint64_t) hi_lo32) << 30) | (uint64_t) lo_lo32;

  { /* Each 9-trit section is 15 bits or less, with 15 bits of buffer between them.
       We have enough room to vectorize division. */
    uint64_t p27_inv = 38837;
    uint64_t mask    = 0x0ffc00003ff00000UL;
    uint64_t hi_hi, lo_hi, hi_hi_hi, lo_hi_hi;

    /* high 6-trit subsections are in *_hi, bits 0:9 and 30:39 */
    hi_hi = ((hi * p27_inv) & mask) >> 20;
    lo_hi = ((lo * p27_inv) & mask) >> 20;

    /* hi_lo and lo_lo each have two 3-trit sections, 0:4 and 30:34.
       hi_hi and lo_hi each have two packed 6-trit sections, 0:9 and 30:39.
       We vectorize one more division to unpack hi_hi and lo_hi. */

    hi_hi_hi = ((hi_hi * p27_inv) & mask) >> 15;
    lo_hi_hi = ((lo_hi * p27_inv) & mask) >> 15;

    /* put the remainder in the low bits (-= x * 27), then
       shift the high subsection to just after the min subsection (|= x << 5),
       combined means += x * 5 */

    /* hi = hi - hi_hi * 27 + (hi_hi + hi_hi_hi * 5) * 2**5 */
    /* hi = hi - hi_hi * 27 + hi_hi * 2**5 + hi_hi_hi * 5 * 2**5 */
    /* hi = hi + (hi_hi + hi_hi_hi * 2**5) * 5 */

    hi += (hi_hi + hi_hi_hi) * 5;
    lo += (lo_hi + lo_hi_hi) * 5;
  }

  /* shift the high 6-subsection word to just after the low 6-subsection word */
  return ((peano_t) hi << 15) | (peano_t) lo;
}

/* Take an unpacked peano representation and convert it back to a digit,
 * where consecutive valid peano representations map to consecutive digits. */
uint64_t
peano_to_digit(peano_t peano)
{
  uint64_t mask = 0x0003e007c00f801fUL;
  uint64_t lo, mid, hi;

  lo      = (uint64_t) peano & mask;
  peano >>= 5;
  mid     = (uint64_t) peano & mask;
  peano >>= 5;
  hi      = (uint64_t) peano & mask;
  lo     += (mid + hi * 27) * 27;

  /* There are now four packed 9-trit groups: at 0:14, 15:29, 30:44, and 45:59 */
  mask   = 0x00001fffc0007fffUL;
  mid    = mask & lo;
  hi     = mask & (lo >> 15);
  mid   += hi * 19683;

  /* There are now two packed 18-trit groups: at 0:28 and 30:58 */
  mask   = 0x000000001fffffffUL;
  lo     = mask & mid;
  hi     = mask & (mid >> 30);

  return lo + hi * 387420489;
}

/* Separate out a peano_t packed into 5-bit representations of 3-trit groups
 * into a separate component for the low, medium, and high trits of each
 * group.  Each 3-trit group is still spaced one per 5 bits. */
void
peano_to_components(peano_t peano,peano_t comps[3])
{
  uint64_t lo_mask = 0xf07c1f07c1f07c1fUL;
  uint64_t hi_mask = 0x0f83e0f83e0f83e0UL;
  uint64_t lo = (uint64_t) peano & lo_mask;       /* get every other group */
  uint64_t lo_div_3 = ((lo * 11) >> 5) & lo_mask; /* perform division by 3 by multiplication (needs workspace, hence every other group) */
  uint64_t hi = (uint64_t) peano & hi_mask;       /* get the remaining groups */
  uint64_t hi_div_3 = ((hi * 11) >> 5) & hi_mask; /* division by 3 by multiplication */

  lo -= lo_div_3 * 3; /* modulo 3 */
  hi -= hi_div_3 * 3; /* modulo 3 */
  comps[0] = lo | hi; /* comp[0] is the lowest trit */

  lo = ((lo_div_3 * 11) >> 5) & lo_mask; /* division by 3 again */
  lo_div_3 -= lo * 3;                    /* modulo 3 again */
  hi = ((hi_div_3 * 11) >> 5) & hi_mask; /* division by 3 again */
  hi_div_3 -= hi * 3;                    /* modulo 3 again */

  comps[1] = lo_div_3 | hi_div_3;        /* comp[1] is the middle trit */
  comps[2] = lo | hi;                    /* comp[3] is the highest trit */
}

/* Take low, medium and high trits, spaced one per 5 bits, and combine them
 * into a peano_t with one 3-trit group per 5-bits. */
peano_t
peano_from_components(peano_t comps[3])
{
  return comps[0] + comps[1] * 3 + comps[2] * 9;
}

/* This simulates the multiplication of two a4 symmetries stored in (vier,
 * zed3) form.  We do not need to explicitly multiply the zed3 components,
 * however, so just the zed3 component for the symmetry on the left is needed,
 * because it transforms the vier of the symmetry on the right. */
#define vz_product(vhi,zhi,v)            \
do {                                     \
  uint64_t _mask = 0x018c6318c6318c63UL; \
  uint64_t _v_orig = v * 3;              \
                                         \
  _v_orig |= _v_orig >> 2;               \
  v += zhi;                              \
  v += v >> 2;                           \
  v &= _mask & _v_orig;                  \
  v ^= vhi;                              \
} while (0);

/* Take the low-, medium-, and high-trit components of a peano_t and convert
 * them into unpacked coordiantes, which can be interpreted as x-, y-, and
 * z-trits (still in unpacked, one trit per 5 bits form). */
void
peano_components_to_unpacked_coordinates(const peano_t comps[3],peano_t excoords[3])
{
  uint64_t x = (uint64_t) comps[0], y = (uint64_t) comps[1], z = (uint64_t) comps[2];
  uint64_t lo_bit = 0x0084210842108421UL;
  uint64_t vier, flip_x, flip_y, flip_z;

  {
    uint64_t midxy = lo_bit & (x ^ y), midyz = lo_bit & (y ^ z), midz = lo_bit & z;

    /* local transformations for deeper levels */
    vier = (midxy) ^ (midyz * 3);

    /* child to coordinate, self */
    flip_x = midyz;
    flip_y = midz;
    x ^= flip_x * 3;
    x -= flip_x;
    y ^= flip_y * 3;
    y -= flip_y;
  }

  {
    /* Prefix product of transformations: we implement a peano curve with a
     * x->y->z->x rotation for each child, because this results in better
     * median-case locality properties.  This means that each initial symmetry
     * is (vier, zed3 = 1).  The result of the prefix multplication on the
     * zed3 component is pre computable for each step, which gives us the
     * magic numbers below. */
    uint64_t vierhi;

    vierhi = vier >> 5;
    vz_product(vierhi,0x0004210842108421UL,vier);
    vierhi = vier >> 10;
    vz_product(vierhi,0x0000221084210842UL,vier);
    vierhi = vier >> 20;
    vz_product(vierhi,0x0000000886108421UL,vier);
    vierhi = vier >> 40;
    vz_product(vierhi,0x0000000000008861UL,vier);
    vier  >>= 5; /* The symmetry applies to one level lower in the tree */
  }

  {
    /* The final form of the zed3 rotations that has been optimized out is
     * that the i^th group is rotated (2 - i) mod 3 times: we have precomputed
     * these rotations here, and use them to rotate directions. */
    uint64_t rot_1 = 0x000c001800300060UL;
    uint64_t rot_2 = 0x00006000c0018003UL;
    uint64_t xy_swap = x ^ y;
    uint64_t xz_swap = x ^ z;
    uint64_t yz_swap = y ^ z;

    x ^= (xz_swap & rot_1) ^ (xy_swap & rot_2);
    y ^= (xy_swap & rot_1) ^ (yz_swap & rot_2);
    z ^= (yz_swap & rot_1) ^ (xz_swap & rot_2);
  }
  /* The viergruppe transforms are (i, x, y, z), where each of the
   * non-identity transforms flips the other two directions */
  flip_x      = lo_bit & (vier >> 1);
  flip_y      = lo_bit &  vier;
  flip_z      = flip_x ^ flip_y;
  x          ^= flip_x * 3;
  y          ^= flip_y * 3;
  z          ^= flip_z * 3;
  excoords[0] = x - flip_x;
  excoords[1] = y - flip_y;
  excoords[2] = z - flip_z;
}

/* Take x-, y-, and z-trits in unpacked, one-trit per 5 bit form, and convert
 * them back into low-, medium-, and high-trit components of a peano_t. */
void
peano_unpacked_coordinates_to_components(const peano_t excoords[3],peano_t comps[3])
{
  uint64_t x = (uint64_t) excoords[0], y = (uint64_t) excoords[1], z = (uint64_t) excoords[2];
  uint64_t lo_bit = 0x0084210842108421UL;
  uint64_t vier, flip_x, flip_y, flip_z;

  {
    uint64_t midxy = lo_bit & (x ^ y), midyz = lo_bit & (y ^ z);

    /* local transformations for deeper levels */
    vier = midxy ^ (midyz * 3);
  }

  /* "Inverse" prefix product of transformations: take the (vier,1) transforms
   * computed from the coordinates, and transform them into the correct
   * transformations that would have been computed from the components.
   *
   * It's a bit interesting that this is simpler than the forward
   * transformation, and deserves some explanation.  The kernel operation is
   * this: suppose that at level i, the contribution to the coordinates are
   * the trits (x, y, z).  If the outer symmetry that changes these trits is
   * the identity, then the inner symmetry that will apply to the (i + 1)^th
   * level is (vier_{(x,y,z)},1), where vier_{(x,y,z)} is computed above.
   *
   * Now suppose that I know that the outer symmetry isn't the identity, it's
   * (vier_hi, zed3_hi).  This means that (x, y, z) is the result of
   * (vier_hi, zed3_hi) applied to the true coordinate system: if I apply
   * (vier_hi, zed3_hi)^{-1} to (x,y,z), I will get the coordinates I should
   * use to compute the inner symmetry.  Symbolically, the inner symmetry
   * should be
   *
   *    (vier_hi, zed_hi) * (vier_{(vier_hi, zed_hi)^{-1}(x,y,z)},1).
   *
   * Here's where it gets interesting:  The way vier is computed from
   * coordinates is that it's lower bit is (x == 1) XOR (z == 1), and its
   * upper bit is (y == 1) XOR (z == 1).  This means that any viergruppe
   * transformation of the (x,y,z) coordinates doesn't change vier_{(x,y,z)},
   * and a zed3 transformation of the (x,y,z) coordinates is
   * an x->y->z->x tranformation applied zed3 times.  Because
   * (vier_hi, zed_hi)^{-1} = zed_hi^{-1} * vier_hi^{-1}, this means that
   *
   *    vier_{(vier_hi, zed_hi)^{-1}(x,y,z)} = zed_hi^{-1} * vier_{(x,y,z}}.
   *
   * Therefore
   *
   *    (vier_hi, zed_hi) * (vier_{(vier_hi, zed_hi)^{-1}(x,y,z)},1)    =
   *    (vier_hi, zed_hi) * (zed_hi^{-1} * vier_{(x,y,z)},1)            =
   *    (vier_hi XOR (zed_hi * zed_hi^{-1} * vier_{(x,y,z)},zed_hi + 1) =
   *    (vier_hi XOR vier_{(x,y,z)}, zed_hi + 1).
   *
   * In otherwords, we just have to prefix XOR the vier components, and prefix
   * add (mod 3) the zed3 components to get the inverse transform.  Since all
   * zed3 components start with 1, we know what the final result will be, so
   * we just have to transform the vier components.
   */
  {
    vier  ^= vier >> 5;
    vier  ^= vier >> 10;
    vier  ^= vier >> 20;
    vier  ^= vier >> 40;
    vier >>= 5;
  }

  /* flip coordinate directions (self inverse) */
  flip_x  = lo_bit & (vier >> 1);
  flip_y  = lo_bit &  vier;
  flip_z  = flip_x ^ flip_y;
  x      ^= flip_x * 3;
  x      -= flip_x;
  y      ^= flip_y * 3;
  y      -= flip_y;
  z      ^= flip_z * 3;
  z      -= flip_z;
  /* rotate coordinate directions, inverse */
  {
    uint64_t rot_1 = 0x000c001800300060UL;
    uint64_t rot_2 = 0x00006000c0018003UL;
    uint64_t xy_swap = x ^ y;
    uint64_t xz_swap = x ^ z;
    uint64_t yz_swap = y ^ z;

    /* Notice that the rotations go in the opposite direction of the forward
     * transformation */
    x ^= (xz_swap & rot_2) ^ (xy_swap & rot_1);
    y ^= (xy_swap & rot_2) ^ (yz_swap & rot_1);
    z ^= (yz_swap & rot_2) ^ (xz_swap & rot_1);

    /* This undoes the local transformation at the start of the forward
     * transformation */
    flip_x = lo_bit & (y ^ z);
    flip_y = lo_bit & z;
    x ^= flip_x * 3;
    y ^= flip_y * 3;
  }
  comps[0] = x - flip_x;
  comps[1] = y - flip_y;
  comps[2] = z;
}

peano_coord_t
peano_pack_coordinate(peano_t excoord)
{
  uint64_t mask = 0x00006000c0018003UL;
  uint64_t lo, mid, hi;

  lo        = (uint64_t) excoord & mask;
  excoord >>= 5;
  mid       = (uint64_t) excoord & mask;
  excoord >>= 5;
  hi        = (uint64_t) excoord & mask;
  lo       += (mid + hi * 3) * 3;

  /* There are now four packed 3-trit groups: at 0:4, 15:19, 30:34, and 45:49 */
  mask   = 0x00000007c000001fUL;
  mid    = mask & lo;
  hi     = mask & (lo >> 15);
  mid   += hi * 27;

  /* There are now two packed 6-trit groups: at 0:9 and 30:39 */
  mask   = 0x00000000000003ffUL;
  lo     = mask & mid;
  hi     = mask & (mid >> 30);

  return (peano_coord_t) lo + (peano_coord_t) hi * 729;
}

peano_t
peano_unpack_coordinate(peano_coord_t coord)
{
  uint64_t p3_6_inv = 1472898;
  uint64_t hi = (p3_6_inv * (uint64_t) coord) & 0x000000ffc0000000UL;
  uint64_t lo = (uint64_t) coord - (hi >> 30) * 729;

  lo |= hi;
  {
    uint64_t p3_3_inv = 1214;

    hi  = (p3_3_inv * lo) & 0x0003e000000f8000UL;
    lo -= (hi >> 15) * 27;
    lo |= hi;
  }
  {
    uint64_t mid = (11 * lo) & 0x007c00f801f003e0UL;

    lo -= (mid >> 5) * 3;
    hi = (11 * mid) & 0x0f801f003e007c00UL;
    mid -= (hi >> 5) * 3;
    return (peano_t) (lo | mid | hi);
  }
}

void
peano_pack_coordinates(const peano_t excoords[3],peano_coord_t coords[3])
{
  for (int i = 0; i < 3; i++) {
    coords[i] = peano_pack_coordinate(excoords[i]);
  }
}

void
peano_unpack_coordinates(const peano_coord_t coords[3],peano_t excoords[3])
{
  for (int i = 0; i < 3; i++) {
    excoords[i] = peano_unpack_coordinate(coords[i]);
  }
}
