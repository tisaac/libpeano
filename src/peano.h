#if !defined(PEANO_H)
#define      PEANO_H
#include <stdint.h>

typedef uint64_t peano_t;
typedef uint32_t peano_coord_t;

#define PEANO_MAX 150094635296999121UL
#define PEANO_COORD_MAX 531441

peano_t       peano_from_digit_reference(uint64_t);
peano_t       peano_from_digit(uint64_t);
uint64_t      peano_to_digit(peano_t);
void          peano_to_components(peano_t,peano_t[3]);
peano_t       peano_from_components(peano_t[3]);
void          peano_components_to_unpacked_coordinates(const peano_t[3],peano_t[3]);
void          peano_unpacked_coordinates_to_components(const peano_t[3],peano_t[3]);
peano_coord_t peano_pack_coordinate(peano_t);
peano_t       peano_unpack_coordinate(peano_coord_t);
void          peano_pack_coordinates(const peano_t[3],peano_coord_t[3]);
void          peano_unpack_coordinates(const peano_coord_t[3],peano_t[3]);

#endif
