#include "peano_test.h"
#include <stdlib.h>
#include <stdio.h>

int
main (int argc, char **argv)
{
  int  test_start, test_end, test_argc = argc - 1;
  char **test_argv;

  test_argv    = (char **) malloc (test_argc * sizeof(*test_argv));
  if (test_argc) {
    test_argv[0] = argv[0];
    for (int i = 2; i < argc; i++) {
      test_argv[i - 1] = argv[i];
    }
  }
  if (argc > 1) {
    test_start = atoi(argv[1]);
    test_end   = test_start + 1;
  }
  else {
    test_start = 0;
    test_end   = peano_test_num_tests;
  }
  printf("%d..%d\n",test_start+1,test_end);
  for (int i = test_start; i < test_end; i++) {
    int err = peano_test(i,test_argc,test_argv);
    if (err) {
      printf ("not ok %d\n",i+1);
    }
    else {
      printf ("ok %d\n",i+1);
    }
  }
  if (test_argv) free(test_argv);
  return 0;
}
