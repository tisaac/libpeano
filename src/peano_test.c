
#include "peano_test.h"
#include "peano.h"
#include <stdlib.h>
#include <stdio.h>

static void
peano_fprintf(FILE *fp,peano_t peano)
{
  for (uint64_t word = peano, i = 0; i < 12; i++, word >>= 5) {
    fprintf(fp," %2d",(int) (word & 31));
  }
}

static uint64_t
random_uint64(void)
{
  static int count = 0;
  static int shift;
  uint64_t   urand64 = 0;

  if (count < 1) {
    uint64_t top_bit = (uint64_t) 1 << 63;

    shift = 64;
    while (shift && !(top_bit & RAND_MAX)) {
      shift--;
      top_bit >>= 1;
    }
    if (64 % shift) {
      count = 1 + (64 / shift);
    }
    else {
      count = 64 / shift;
    }
  }
  for (int i = 0; i < count; i++) {
    urand64 ^= ((uint64_t) rand()) << (i * shift);
  }
  return urand64;
}

static int
peano_test_from_digit_single (uint64_t digit)
{
  peano_t  peano_ref, peano;
  uint64_t check_digit;

  peano_ref = peano_from_digit_reference(digit);
  peano     = peano_from_digit(digit);
  if (peano_ref != peano) {
    fprintf(stderr,"# %s(), peano_from_digit_reference mismatch: digit %lu\n",__func__,digit);
    fprintf(stderr,"# reference  %lu:",peano_ref);
    peano_fprintf(stderr,peano_ref);
    fprintf(stderr,"\n");
    fprintf(stderr,"# vectorized %lu:",peano);
    peano_fprintf(stderr,peano);
    fprintf(stderr,"\n");
    return -1;
  }
  check_digit = peano_to_digit(peano);
  if (check_digit != digit) {
    fprintf(stderr,"# %s(), peano_from_digit / peano_to_digit mismatch: digit %lu\n",__func__,digit);
    fprintf(stderr,"# peano       %lu:",peano);
    peano_fprintf(stderr,peano);
    fprintf(stderr,"\n");
    fprintf(stderr,"# check_digit %lu\n",check_digit);
    return -1;
  }
  return 0;
}

static int
peano_test_from_digit (int argc, char **argv)
{
  int numTests = 1000000;
  int seed     = 0;

  if (argc > 1) numTests = atoi(argv[1]);
  if (argc > 2) seed     = atoi(argv[2]);
  for (int i = 3; i < argc; i++) {
    uint64_t digit = strtoul(argv[i],NULL,0);
    int      err   = peano_test_from_digit_single(digit);
    if (err) return err;
  }
  srand(seed);
  for (int i = 0; i < numTests; i++) {
    uint64_t digit = random_uint64() % PEANO_MAX;
    int      err   = peano_test_from_digit_single(digit);
    if (err) return err;
  }
  return 0;
}

static int
peano_test_to_components_single (uint64_t digit)
{
  peano_t  peano, peano_check;
  peano_t  comps[3];

  peano = peano_from_digit(digit);
  peano_to_components(peano,comps);
  peano_check = peano_from_components(comps);
  if (peano_check != peano) {
    fprintf(stderr,"# %s(), peano_to_components / peano_from_components mismatch: digit %lu\n",__func__,digit);
    fprintf(stderr,"# input %lu:",peano);
    peano_fprintf(stderr,peano);
    fprintf(stderr,"\n");
    for (int j = 0; j < 3; j++) {
      fprintf(stderr,"# comps[%d]:",j);
      peano_fprintf(stderr,comps[j]);
      fprintf(stderr,"\n");
    }
    fprintf(stderr,"# check %lu:",peano_check);
    peano_fprintf(stderr,peano_check);
    fprintf(stderr,"\n");
    return -1;
  }
  return 0;
}

static int
peano_test_to_components (int argc, char **argv)
{
  int numTests = 1000000;
  int seed     = 0;

  if (argc > 1) numTests = atoi(argv[1]);
  if (argc > 2) seed     = atoi(argv[2]);
  for (int i = 3; i < argc; i++) {
    uint64_t digit = strtoul(argv[i],NULL,0);
    int      err   = peano_test_to_components_single(digit);
    if (err) return err;
  }
  srand(seed);
  for (int i = 0; i < numTests; i++) {
    uint64_t digit = random_uint64() % PEANO_MAX;
    int      err   = peano_test_to_components_single(digit);
    if (err) return err;
  }
  return 0;
}

static int
peano_test_components_to_unpacked_coordinates_single (uint64_t digit)
{
  peano_t  peano;
  peano_t  comps[3], excoords[3], comps_check[3];

  peano = peano_from_digit(digit);
  peano_to_components(peano,comps);
  peano_components_to_unpacked_coordinates(comps,excoords);
  peano_unpacked_coordinates_to_components(excoords,comps_check);
  if (comps[0] != comps_check[0] || comps[1] != comps_check[1] || comps[2] != comps_check[2]) {
    fprintf(stderr,"# %s(), peano_components_to_unpacked_coordinates / peano_unpacked_coordinates_to_components mismatch: digit %lu\n",__func__,digit);
    fprintf(stderr,"# input %lu:",peano);
    peano_fprintf(stderr,peano);
    fprintf(stderr,"\n");
    for (int j = 0; j < 3; j++) {
      fprintf(stderr,"# comps[%d]:      ",j);
      peano_fprintf(stderr,comps[j]);
      fprintf(stderr,"\n");
    }
    for (int j = 0; j < 3; j++) {
      fprintf(stderr,"# excoords[%d]:   ",j);
      peano_fprintf(stderr,excoords[j]);
      fprintf(stderr,"\n");
    }
    for (int j = 0; j < 3; j++) {
      fprintf(stderr,"# comps_check[%d]:",j);
      peano_fprintf(stderr,comps_check[j]);
      fprintf(stderr,"\n");
    }
    return -1;
  }
  return 0;
}

static int
peano_test_components_to_unpacked_coordinates (int argc, char **argv)
{
  int numTests = 1000000;
  int seed     = 0;

  if (argc > 1) numTests = atoi(argv[1]);
  if (argc > 2) seed     = atoi(argv[2]);
  for (int i = 3; i < argc; i++) {
    uint64_t digit = strtoul(argv[i],NULL,0);
    int      err   = peano_test_components_to_unpacked_coordinates_single(digit);
    if (err) return err;
  }
  srand(seed);
  for (int i = 0; i < numTests; i++) {
    uint64_t digit = random_uint64() % PEANO_MAX;
    int      err   = peano_test_components_to_unpacked_coordinates_single(digit);
    if (err) return err;
  }
  return 0;
}

static int
peano_test_pack_coordinates_single (uint64_t digit)
{
  peano_t       peano;
  peano_t       comps[3], excoords[3], excoords_check[3];
  peano_coord_t coords[3];

  peano = peano_from_digit(digit);
  peano_to_components(peano,comps);
  peano_components_to_unpacked_coordinates(comps,excoords);
  peano_pack_coordinates(excoords,coords);
  peano_unpack_coordinates(coords,excoords_check);
  if (excoords[0] != excoords_check[0] || excoords[1] != excoords_check[1] || excoords[2] != excoords_check[2]) {
    fprintf(stderr,"# %s(), peano_components_to_unpacked_coordinates / peano_unpacked_coordinates_to_components mismatch: digit %lu\n",__func__,digit);
    fprintf(stderr,"# input %lu:",peano);
    peano_fprintf(stderr,peano);
    fprintf(stderr,"\n");
    for (int j = 0; j < 3; j++) {
      fprintf(stderr,"# comps[%d]:",j);
      peano_fprintf(stderr,comps[j]);
      fprintf(stderr,"\n");
    }
    for (int j = 0; j < 3; j++) {
      fprintf(stderr,"# excoords[%d]:",j);
      peano_fprintf(stderr,excoords[j]);
      fprintf(stderr,"\n");
    }
    for (int j = 0; j < 3; j++) {
      fprintf(stderr,"# coords[%d]: %u\n",j,coords[j]);
    }
    for (int j = 0; j < 3; j++) {
      fprintf(stderr,"# excoords_check[%d]:",j);
      peano_fprintf(stderr,excoords_check[j]);
      fprintf(stderr,"\n");
    }
    return -1;
  }
  return 0;
}

static int
peano_test_pack_coordinates (int argc, char **argv)
{
  int numTests = 1000000;
  int seed     = 0;

  if (argc > 1) numTests = atoi(argv[1]);
  if (argc > 2) seed     = atoi(argv[2]);
  for (int i = 3; i < argc; i++) {
    uint64_t digit = strtoul(argv[i],NULL,0);
    int      err   = peano_test_pack_coordinates_single(digit);
    if (err) return err;
  }
  srand(seed);
  for (int i = 0; i < numTests; i++) {
    uint64_t digit = random_uint64() % PEANO_MAX;
    int      err   = peano_test_pack_coordinates_single(digit);
    if (err) return err;
  }
  return 0;
}

static int
peano_test_vtk (int argc, char **argv) {
  int        level = 3;
  uint64_t   total = 1;
  FILE       *fp;
  const char *filename = "peano.vtk";

  if (argc > 1) level    = atoi(argv[1]);
  if (argc > 2) filename = argv[2];
  level = level > 12 ? 12 : level;
  for (int i = 0; i < level; i++) {
    total *= 27;
  }
  fp = fopen (filename, "w");
  fprintf (fp, "# vtk DataFile Version 2.0\n");
  fprintf (fp, "Test 3d Peano curve construction\n");
  fprintf (fp, "ASCII\n");
  fprintf (fp, "DATASET POLYDATA\n");
  fprintf (fp, "POINTS %lu int\n", total);
  for (uint64_t i = 0; i < total; i++) {
    peano_t       peano = peano_from_digit(i);
    peano_t       comps[3], excoords[3];
    peano_coord_t coords[3];

    peano_to_components(peano,comps);
    peano_components_to_unpacked_coordinates(comps,excoords);
    peano_pack_coordinates(excoords,coords);
    fprintf (fp, "%d %d %d\n", (int) coords[0], (int) coords[1], (int) coords[2]);
  }
  fprintf (fp, "LINES %d %d\n", (int) (total - 1), (int) (3 * (total - 1)));
  for (uint64_t i = 1; i < total; i++) {
    fprintf (fp, "2 %d %d\n", (int) i-1, (int)i);
  }
  fprintf (fp, "\n");
  fclose(fp);
  return 0;
}

#define PEANO_TEST_NUM_TESTS 5
static int (*peano_tests[PEANO_TEST_NUM_TESTS])(int,char**) =
{
  peano_test_from_digit,
  peano_test_to_components,
  peano_test_components_to_unpacked_coordinates,
  peano_test_pack_coordinates,
  peano_test_vtk,
};
const int peano_test_num_tests = PEANO_TEST_NUM_TESTS;

int
peano_test (int test_num, int argc, char **argv)
{
  if (test_num >= 0 && test_num < peano_test_num_tests) {
    int err = peano_tests[test_num](argc,argv);
    if (err) {
      return err;
    }
  }
  else {
    fprintf(stderr,"# %s, %s(): no test number %d (%d tests).\n",argv[0],__func__,test_num,peano_test_num_tests);
    return 1;
  }
  return 0;
}
